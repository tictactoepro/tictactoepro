﻿namespace TicTacToeServer
{
    partial class ServerForm1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblServerConfigTitle = new System.Windows.Forms.Label();
            this.listBoxServerMessages = new System.Windows.Forms.ListBox();
            this.comboIPAddress = new System.Windows.Forms.ComboBox();
            this.lblIPAddressTitle = new System.Windows.Forms.Label();
            this.numericPort = new System.Windows.Forms.NumericUpDown();
            this.lblPortTitle = new System.Windows.Forms.Label();
            this.btnStartServer = new System.Windows.Forms.Button();
            this.txtChatHistory = new System.Windows.Forms.TextBox();
            this.lblChatHistoryTitle = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNewMessage = new System.Windows.Forms.TextBox();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.bwConnectionListener = new System.ComponentModel.BackgroundWorker();
            this.bwConnectionHandler = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.numericPort)).BeginInit();
            this.SuspendLayout();
            // 
            // lblServerConfigTitle
            // 
            this.lblServerConfigTitle.AutoSize = true;
            this.lblServerConfigTitle.Location = new System.Drawing.Point(12, 9);
            this.lblServerConfigTitle.Name = "lblServerConfigTitle";
            this.lblServerConfigTitle.Size = new System.Drawing.Size(145, 17);
            this.lblServerConfigTitle.TabIndex = 0;
            this.lblServerConfigTitle.Text = "Konfiguracja serwera:";
            // 
            // listBoxServerMessages
            // 
            this.listBoxServerMessages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxServerMessages.FormattingEnabled = true;
            this.listBoxServerMessages.ItemHeight = 16;
            this.listBoxServerMessages.Location = new System.Drawing.Point(15, 38);
            this.listBoxServerMessages.Name = "listBoxServerMessages";
            this.listBoxServerMessages.Size = new System.Drawing.Size(484, 100);
            this.listBoxServerMessages.TabIndex = 1;
            // 
            // comboIPAddress
            // 
            this.comboIPAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboIPAddress.FormattingEnabled = true;
            this.comboIPAddress.Location = new System.Drawing.Point(93, 148);
            this.comboIPAddress.Name = "comboIPAddress";
            this.comboIPAddress.Size = new System.Drawing.Size(176, 24);
            this.comboIPAddress.TabIndex = 2;
            this.comboIPAddress.Text = "127.0.0.1";
            this.comboIPAddress.TextChanged += new System.EventHandler(this.ComboIPAddress_TextChanged);
            // 
            // lblIPAddressTitle
            // 
            this.lblIPAddressTitle.AutoSize = true;
            this.lblIPAddressTitle.Location = new System.Drawing.Point(12, 151);
            this.lblIPAddressTitle.Name = "lblIPAddressTitle";
            this.lblIPAddressTitle.Size = new System.Drawing.Size(65, 17);
            this.lblIPAddressTitle.TabIndex = 3;
            this.lblIPAddressTitle.Text = "Adres IP:";
            // 
            // numericPort
            // 
            this.numericPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numericPort.Location = new System.Drawing.Point(319, 148);
            this.numericPort.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.numericPort.Name = "numericPort";
            this.numericPort.Size = new System.Drawing.Size(82, 22);
            this.numericPort.TabIndex = 4;
            this.numericPort.Value = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            // 
            // lblPortTitle
            // 
            this.lblPortTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPortTitle.AutoSize = true;
            this.lblPortTitle.Location = new System.Drawing.Point(275, 150);
            this.lblPortTitle.Name = "lblPortTitle";
            this.lblPortTitle.Size = new System.Drawing.Size(38, 17);
            this.lblPortTitle.TabIndex = 5;
            this.lblPortTitle.Text = "Port:";
            // 
            // btnStartServer
            // 
            this.btnStartServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartServer.Location = new System.Drawing.Point(424, 149);
            this.btnStartServer.Name = "btnStartServer";
            this.btnStartServer.Size = new System.Drawing.Size(75, 23);
            this.btnStartServer.TabIndex = 6;
            this.btnStartServer.Text = "Start";
            this.btnStartServer.UseVisualStyleBackColor = true;
            this.btnStartServer.Click += new System.EventHandler(this.BtnStartServer_Click);
            // 
            // txtChatHistory
            // 
            this.txtChatHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChatHistory.Location = new System.Drawing.Point(15, 220);
            this.txtChatHistory.Multiline = true;
            this.txtChatHistory.Name = "txtChatHistory";
            this.txtChatHistory.ReadOnly = true;
            this.txtChatHistory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtChatHistory.ShortcutsEnabled = false;
            this.txtChatHistory.Size = new System.Drawing.Size(484, 126);
            this.txtChatHistory.TabIndex = 7;
            // 
            // lblChatHistoryTitle
            // 
            this.lblChatHistoryTitle.AutoSize = true;
            this.lblChatHistoryTitle.Location = new System.Drawing.Point(12, 191);
            this.lblChatHistoryTitle.Name = "lblChatHistoryTitle";
            this.lblChatHistoryTitle.Size = new System.Drawing.Size(137, 17);
            this.lblChatHistoryTitle.TabIndex = 8;
            this.lblChatHistoryTitle.Text = "Historia konwersacji:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 361);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Nowa wiadomość:";
            // 
            // txtNewMessage
            // 
            this.txtNewMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewMessage.Location = new System.Drawing.Point(15, 390);
            this.txtNewMessage.Multiline = true;
            this.txtNewMessage.Name = "txtNewMessage";
            this.txtNewMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNewMessage.ShortcutsEnabled = false;
            this.txtNewMessage.Size = new System.Drawing.Size(484, 73);
            this.txtNewMessage.TabIndex = 9;
            this.txtNewMessage.TextChanged += new System.EventHandler(this.txtNewMessage_TextChanged);
            this.txtNewMessage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxtNewMessage_KeyPress);
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendMessage.Location = new System.Drawing.Point(424, 469);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(75, 23);
            this.btnSendMessage.TabIndex = 11;
            this.btnSendMessage.Text = "Wyślij";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.BtnSendMessage_Click);
            // 
            // bwConnectionListener
            // 
            this.bwConnectionListener.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BwConnectionListener_DoWork);
            // 
            // bwConnectionHandler
            // 
            this.bwConnectionHandler.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BwConnectionHandler_DoWork);
            // 
            // ServerForm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 496);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNewMessage);
            this.Controls.Add(this.lblChatHistoryTitle);
            this.Controls.Add(this.txtChatHistory);
            this.Controls.Add(this.btnStartServer);
            this.Controls.Add(this.lblPortTitle);
            this.Controls.Add(this.numericPort);
            this.Controls.Add(this.lblIPAddressTitle);
            this.Controls.Add(this.comboIPAddress);
            this.Controls.Add(this.listBoxServerMessages);
            this.Controls.Add(this.lblServerConfigTitle);
            this.Name = "ServerForm1";
            this.Text = "Komunikator - Serwer";
            ((System.ComponentModel.ISupportInitialize)(this.numericPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblServerConfigTitle;
        private System.Windows.Forms.ListBox listBoxServerMessages;
        private System.Windows.Forms.ComboBox comboIPAddress;
        private System.Windows.Forms.Label lblIPAddressTitle;
        private System.Windows.Forms.NumericUpDown numericPort;
        private System.Windows.Forms.Label lblPortTitle;
        private System.Windows.Forms.Button btnStartServer;
        private System.Windows.Forms.TextBox txtChatHistory;
        private System.Windows.Forms.Label lblChatHistoryTitle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNewMessage;
        private System.Windows.Forms.Button btnSendMessage;
        private System.ComponentModel.BackgroundWorker bwConnectionListener;
        private System.ComponentModel.BackgroundWorker bwConnectionHandler;
    }
}

