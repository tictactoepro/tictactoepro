﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

namespace TicTacToeServer
{
    public partial class ServerForm1 : Form
    {
        //stala z domyslnym adresem IP (lokalny adres)
        private const string DefaultIpAddress = "127.0.0.1";

        //stale z początkowym i koncowym łańcuchem znaków
        private const string InviteCode = "###START###";
        private const string ByeByeCode = "###KONIEC###";

        //nasluchiwacz polaczenia
        private TcpListener _tcpListener;

        //klient polaczenia
        private TcpClient _tcpClient;

        //adres IP nasluchu
        private string _ipAddress = DefaultIpAddress;

        //odczyt danych
        private BinaryReader _reader;

        //wysylka danych
        private BinaryWriter _writer;

        private bool _isConnectionActive;

        public ServerForm1()
        {
            InitializeComponent();
            //wypelnienie listy adresow IP danymi pobranymi z naszego komputera
            IPHostEntry ipHostEntry = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ipAddress in ipHostEntry.AddressList)
            {
                comboIPAddress.Items.Add(ipAddress.ToString());
            }
        }

        //przykład delegatu
        delegate void SetTextCallBack(string text);

        //wykorzystanie delegatu dla kontrolki z historia komunikatów serwera
        private void SetServerMessagesText(string text)
        {
            if (listBoxServerMessages.InvokeRequired)
            {
                SetTextCallBack setTextCallback = SetServerMessagesText;
                Invoke(setTextCallback, text);
            }
            else
            {
                listBoxServerMessages.Items.Add(text);
            }
        }

        //wykorzystanie delegatu dla kontrolki z historią wiadomości
        private void SetChatHistoryText(string text)
        {
            if (txtChatHistory.InvokeRequired)
            {
                SetTextCallBack setTextCallback = SetChatHistoryText;
                Invoke(setTextCallback, text);
            }
            else
            {
                txtChatHistory.AppendText(text);
            }
        }

        //wpisanie nowej wiadomości do historii chatu z wykorzystaniem metod powyżej
        private void PutMessageToChatHistory(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                // dla pustego tekstu lub białych znaków nic nie robimy
                return;
            }
            //znak nowej linii
            if (txtChatHistory.Text != string.Empty)
                SetChatHistoryText(Environment.NewLine);

            //wpisanie tekstu w kontrolkę, wyciecie bialych znakow na poczatku i koncu
            SetChatHistoryText(text.Trim());
        }

        private void ComboIPAddress_TextChanged(object sender, EventArgs e)
        {
            //w momencie zmiany adresu IP na liście zmieniamy adres IP nasluchu
            _ipAddress = comboIPAddress.Text;
        }

        private void BwConnectionListener_DoWork(object sender, DoWorkEventArgs e)
        {
            IPAddress serverCurrentIpAddress;
            try
            {
                //weryfikacja adresu IP do nasłuchu
                serverCurrentIpAddress = IPAddress.Parse(_ipAddress);
            }
            catch
            {
                //w przypadku weryfikacji negatywnej (wystąpienie wyjątku) wyświetlamy stosowny komunikat i kończymy połączenie
                MessageBox.Show("Błędny adres IP...");
                _isConnectionActive = false;
                return;
            }
            //konfiguracja nasłuchu na wskazanym adresie IP i porcie
            _tcpListener = new TcpListener(serverCurrentIpAddress, (int)numericPort.Value);
            try
            {
                //rozpoczęcie nasłuchu 
                _tcpListener.Start();
                //wyświetlenie komunikatu o rozpoczęciu nasłuchu
                SetServerMessagesText("Oczekiwanie na połączenie ...");
                //przyjęcie połączenia w przypadku jego przyjścia
                _tcpClient = _tcpListener.AcceptTcpClient();
                //strumień danych od Klienta
                NetworkStream networkStream = _tcpClient.GetStream();
                //wyświetlenie komunikatu o nadejściu połączenia
                SetServerMessagesText("Próba połączenia ze strony Klienta...");
                //spięcie zapisu i odczytu ze strumieniem danych od Klienta
                _reader = new BinaryReader(networkStream);
                _writer = new BinaryWriter(networkStream);
                //sprawdzenie czy pierwszy komunikat od Klienta odpowiada oczekiwanemu kodowi uwierzytelnienia
                if (IsStartMessage(_reader.ReadString()))
                {
                    //jeżeli tak - wyświetlenie komunikatu i uruchomienie wątku odpowiedzialnego za obsługę wiadomości
                    SetServerMessagesText("Klient uwierzytelniony poprawnie... Połączenie powiodło się.");
                    bwConnectionHandler.RunWorkerAsync();
                }
                else
                {
                    //jeżeli nie - wyświetlenie komunikatu i przerwanie połączenia
                    SetServerMessagesText("Klient nie uwierzytelnił się poprawnie... Połączenie zostało przerwane.");
                    _tcpClient.Close();
                    _tcpListener.Stop();
                    _isConnectionActive = false;
                }
            }
            catch (Exception ex)
            {
                //obsługa wyjątku - wyświetlamy komunikat i dezaktywujemy połączenie
                SetServerMessagesText("Wystąpił błąd... Połączenie zostało przerwane.");
                SetServerMessagesText($"Komunikat błędu: {ex.Message}");
                _isConnectionActive = false;
            }
        }

        private bool IsStartMessage(string message) => message.Equals(InviteCode);

        private void BwConnectionHandler_DoWork(object sender, DoWorkEventArgs e)
        {
            //deklaracja zmiennej
            string message;
            try
            {
                //dopoki przychodzaca wiadomosc od Klienta nie jest pozegnaniem
                while (!IsEndMessage(out message))
                {
                    //dopisuj wiadomości do okienka konwersacji
                    PutMessageToChatHistory($"Klient: {message}");
                }
                //wyswietlanie komunikatu o pozegnaniu
                SetServerMessagesText("Połączenie zakończone przez Klienta.");
            }
            catch (Exception ex)
            {
                //obsługa wyjątku - wyświetlamy komunikat i dezaktywujemy połączenie
                SetServerMessagesText("Wystąpił błąd... Połączenie zostało przerwane.");
                SetServerMessagesText($"Komunikat błędu: {ex.Message}");
            }
            finally
            {
                //blok finally - wykona się zawsze na końcu
                _isConnectionActive = false;
                _tcpClient.Close();
                _tcpListener.Stop();
                btnStartServer.Text = "Start";
            }
        }

        private bool IsEndMessage(out string message)
        {
            message = _reader.ReadString();
            return message.Equals(ByeByeCode);
        }

        private void BtnStartServer_Click(object sender, EventArgs e)
        {
            try
            {
                if (!_isConnectionActive)
                    HandleServerStart();
                else
                    HandleServerStop();
            }
            catch (Exception ex)
            {
                SetServerMessagesText("Wystąpił błąd...");
                SetServerMessagesText($"Komunikat błędu: {ex.Message}");
                btnStartServer.Text = "Start";
            }
        }

        private void HandleServerStart()
        {
            _isConnectionActive = true;
            bwConnectionListener.RunWorkerAsync();
            btnStartServer.Text = "Stop";
        }

        private void HandleServerStop()
        {
            //oznaczenie polaczenia jako nieaktywnego
            _isConnectionActive = false;
            if (_tcpClient != null)
            {
                //zatrzymanie klienta tcp
                _tcpClient.Close();
            }
            if (_tcpListener != null)
            {
                //zatrzymanie nasluchu
                _tcpListener.Stop();
            }
            if (bwConnectionListener.IsBusy)
            {
                //zatrzymanie watku odpowiedzialnego za nasluch
                bwConnectionListener.CancelAsync();
            }
            if (bwConnectionHandler.IsBusy)
            {
                //zatrzymanie watku odpowiedzialnego za wiadomości
                bwConnectionHandler.CancelAsync();
            }
            //zmiana etykiety przycisku
            btnStartServer.Text = "Start";
        }

        private void BtnSendMessage_Click(object sender, EventArgs e)
        {
            //jezeli polaczenie jest aktywne - wysylamy wiadomosc
            if (_isConnectionActive)
            {
                //wysylka wiadomosci do strumienia Klienta
                _writer.Write(txtNewMessage.Text);
                //wpisanie wiadomosci w historie konwersacji
                PutMessageToChatHistory($"Serwer: {txtNewMessage.Text}");
                //wyczyszczenie pola do wysylki wiadomosci
                txtNewMessage.Clear();
            }
        }

        private void TxtNewMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            //wysylka wiadomosci tez na enter
            if (e.KeyChar == (int)Keys.Enter)
            {
                BtnSendMessage_Click(sender, e);
            }
        }

        private void txtNewMessage_TextChanged(object sender, EventArgs e)
        {
            if(txtNewMessage.Text == Environment.NewLine)
                txtNewMessage.Clear();
        }
    }
}
