﻿using System;
using TicTacToeCore.Enums;
using TicTacToeCore.Models;

namespace TicTacToeCore.Helpers
{
    public static class PointByDimensionTypeHelper
    {
        /// <summary>
        /// Returns point by ComplexTestType and dimensions.
        /// </summary>
        public static Board.Point GetPoint(DimensionType dimensionType, int firstDimensionIndex, int secondDimensionIndex)
        {
            switch (dimensionType)
            {
                case DimensionType.Rows:
                    return new Board.Point(secondDimensionIndex, firstDimensionIndex);
                case DimensionType.Columns:
                    return new Board.Point(firstDimensionIndex, secondDimensionIndex);
                case DimensionType.DiagonalLeftUp:
                    return new Board.Point(secondDimensionIndex, secondDimensionIndex);
                case DimensionType.DiagonalRightUp:
                    return new Board.Point(secondDimensionIndex, GetRightUpDiagonalColumnIndex(secondDimensionIndex));
                default:
                    throw new ArgumentException($"Method doesn't handle {dimensionType}.");
            }
        }

        private static int GetRightUpDiagonalColumnIndex(int diagonalRowIndex) =>
            diagonalRowIndex == 0 ? 2 : diagonalRowIndex == 1 ? 1 : 0;
    }
}
