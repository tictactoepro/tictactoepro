﻿using System.Collections.Generic;
using System.Linq;
using TicTacToeCore.Enums;
using TicTacToeCore.Models;

namespace TicTacToeCore.Helpers
{
    public static class BoardHelper
    {
        public static Board GetEmptyBoard() =>
            Board.GetBoard(new [,]
            {
                { PlayerSign.None, PlayerSign.None, PlayerSign.None },
                { PlayerSign.None, PlayerSign.None, PlayerSign.None },
                { PlayerSign.None, PlayerSign.None, PlayerSign.None }
            });

        public static Board GetFilledBoard(PlayerSign[,] fields) => Board.GetBoard(fields);

        public static int GetEmptyFieldsCount(Board board)
        {
            int emptyFieldsCount = 0;

            foreach (PlayerSign field in board.Fields)
            {
                if (field == PlayerSign.None)
                {
                    emptyFieldsCount++;
                }
            }

            return emptyFieldsCount;
        }
    }
}
