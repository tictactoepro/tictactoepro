﻿using TicTacToeCore.Models;
using TicTacToeCore.Enums;
using System.Collections.Generic;
using System;

namespace TicTacToeCore.Services
{
    public class GameResultService
    {
        private const int MinimumMovesCountToFinishGame = 5;
        public GameResult CheckGameResult(Board board)
        {
            GameResult gameResult = new GameResult();

            if (board.EmptyFieldsCount < MinimumMovesCountToFinishGame)
            {
                List<Func<Board, GameResult>> gameCheckMethods = new List<Func<Board, GameResult>>
                {
                    CheckColumns,
                    CheckRows,
                    CheckUpperLeftToLowerRight,
                    CheckUpperRightToLowerLeft,
                };

                foreach (Func<Board, GameResult> currentGameCheckMethod in gameCheckMethods)
                {
                    gameResult = currentGameCheckMethod(board);

                    if (gameResult.IsGameOver)
                        break;
                }

                if (!gameResult.IsGameOver && board.NoMovesLeft)
                {
                    gameResult.ResultSign = PlayerSign.None; //Draw
                }
            }

            return gameResult;
        }

        private GameResult CheckColumns(Board board)
        {
            GameResult gameResult = new GameResult();
            for (int columnIndex = 0; columnIndex < 3; columnIndex++)
            {
                if (board.Fields[0, columnIndex].Equals(board.Fields[1, columnIndex]) &&
                    board.Fields[0, columnIndex].Equals(board.Fields[2, columnIndex]) &&
                    board.Fields[0, columnIndex] != PlayerSign.None)
                {
                    gameResult.ResultSign = board.Fields[0, columnIndex];
                    break;
                }
            }

            return gameResult;
        }

        private GameResult CheckRows(Board board)
        {
            GameResult gameResult = new GameResult();

            for (int rowIndex = 0; rowIndex < 3; rowIndex++)
            {
                if (board.Fields[rowIndex, 0].Equals(board.Fields[rowIndex, 1]) &&
                    board.Fields[rowIndex, 0].Equals(board.Fields[rowIndex, 2]) &&
                    board.Fields[rowIndex, 0] != PlayerSign.None)
                {
                    gameResult.ResultSign = board.Fields[rowIndex, 0];
                    break;
                }
            }

            return gameResult;
        }

        private GameResult CheckUpperLeftToLowerRight(Board board)
        {
            GameResult gameResult = new GameResult();

            if (board.Fields[0, 0].Equals(board.Fields[1, 1]) &&
                board.Fields[0, 0].Equals(board.Fields[2, 2]) &&
                board.Fields[0, 0] != PlayerSign.None)
            {
                gameResult.ResultSign = board.Fields[0, 0];
            }

            return gameResult;
        }

        private GameResult CheckUpperRightToLowerLeft(Board board)
        {
            GameResult gameResult = new GameResult();

            if (board.Fields[2, 0].Equals(board.Fields[1, 1]) &&
                board.Fields[2, 0].Equals(board.Fields[0, 2]) &&
                board.Fields[2, 0] != PlayerSign.None)
            {
                gameResult.ResultSign = board.Fields[2, 0];
            }

            return gameResult;
        }
    }
}