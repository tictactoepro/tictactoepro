﻿using System;
using TicTacToeCore.Enums;
using TicTacToeCore.Extensions;
using TicTacToeCore.Helpers;
using System.Collections.Generic;
using TicTacToeCore.Models;

namespace TicTacToeCore.Services
{
    public class AIMovesService
    {
        public Board.Point GetNextMovePoint(PlayerSign[,] fields, PlayerSign aiSign)
        {
            Board.Point point;

            List<Func<PlayerSign[,], PlayerSign, Board.Point>> winningPointFunctions =
                new List<Func<PlayerSign[,], PlayerSign, Board.Point>>
                {
                    GetWonInRowPoint,
                    GetWonInColumnPoint,
                    GetWonFromLeftUpDiagonalPoint,
                    GetWonFromRightUpDiagonalPoint,
                };

            List<Func<PlayerSign[,], PlayerSign, Board.Point>> blockingPointFunctions =
                new List<Func<PlayerSign[,], PlayerSign, Board.Point>>
                {
                    GetBlockInRowPoint,
                    GetBlockInColumnPoint,
                    GetBlockFromLeftUpDiagonalPoint,
                    GetBlockFromRightUpDiagonalPoint,
                };

            foreach (Func<PlayerSign[,], PlayerSign, Board.Point> currentWinningPointFunction in winningPointFunctions)
            {
                point = currentWinningPointFunction(fields, aiSign);

                if (point != null)
                    return point;
            }

            foreach (Func<PlayerSign[,], PlayerSign, Board.Point> currentBlockingPointFunction in blockingPointFunctions)
            {
                point = currentBlockingPointFunction(fields, aiSign);

                if (point != null)
                    return point;
            }
            
            return point = GenerateRandomAvailableFieldPoint(fields, aiSign);
        }

        private Board.Point GenerateRandomAvailableFieldPoint(PlayerSign[,] fields, PlayerSign sign)
        {
            var randomIndexGenerator = new Random();
            int rowIndex = randomIndexGenerator.Next(0, 2);
            int columnIndex = randomIndexGenerator.Next(0, 2);

            while (fields[rowIndex, columnIndex] != PlayerSign.None)
            {
                rowIndex = randomIndexGenerator.Next(0, 2);
                columnIndex = randomIndexGenerator.Next(0, 2);
            };

            return new Board.Point(columnIndex, rowIndex);
        }

        public Board.Point GetWonInRowPoint(PlayerSign[,] fields, PlayerSign sign)
        {
            for (int rowIndex = 0; rowIndex < 3; rowIndex++)
            {
                int signsCount = 0;
                int emptyFieldsCount = 0;

                for (int columnIndex = 0; columnIndex < 3; columnIndex++)
                {
                    var currentField = fields[rowIndex, columnIndex];

                    if (currentField == sign)
                        signsCount++;
                    else if (currentField == PlayerSign.None)
                        emptyFieldsCount++;
                }

                if (signsCount == 2 && emptyFieldsCount == 1)
                    return new Board.Point(GetSecondDimensionWithoutSignIndex(fields, DimensionType.Rows, rowIndex), rowIndex);
            }

            return null;
        }

        /// <summary>
        /// Returns first empty field's (with value None) second (if the given is row returns column and vice versa) dimension's index.
        /// If doesn't find any empty field returns -1.
        /// </summary>
        /// <param name="firstDimensionIndex">For rows row index</param>
        /// <returns></returns>
        private int GetSecondDimensionWithoutSignIndex(PlayerSign[,] fields, DimensionType dimensionType, int firstDimensionIndex)
        {
            //ROW, COLUMN
            Board.Point[] points = new Board.Point[3];
            for (int i = 0; i < 3; i++)
            {
                points[i] = PointByDimensionTypeHelper.GetPoint(dimensionType, firstDimensionIndex, i);
                if (fields[points[i].Y, points[i].X] == PlayerSign.None)
                    return points[i].X != firstDimensionIndex ? points[i].X : points[i].Y;
            }

            return -1;
        }

        //TODO:You can implement this method really clever I count on your intelligence Kappa
        public Board.Point GetBlockInRowPoint(PlayerSign[,] fields, PlayerSign sign) => GetWonInRowPoint(fields, sign.GetOpponentSign());

        public Board.Point GetWonInColumnPoint(PlayerSign[,] fields, PlayerSign sign)
        {
            for (int columnIndex = 0; columnIndex < 3; columnIndex++)
            {
                int signsCount = 0;
                int emptyFieldsCount = 0;

                for (int rowIndex = 0; rowIndex < 3; rowIndex++)
                {
                    var currentField = fields[rowIndex, columnIndex];

                    if (currentField == sign)
                        signsCount++;
                    else if (currentField == PlayerSign.None)
                        emptyFieldsCount++;
                }

                if (signsCount == 2 && emptyFieldsCount == 1)
                    return new Board.Point(columnIndex, GetSecondDimensionWithoutSignIndex(fields, DimensionType.Columns, columnIndex));
            }

            return null;
        }

        public Board.Point GetBlockInColumnPoint(PlayerSign[,] fields, PlayerSign sign) =>
            GetWonInColumnPoint(fields, sign.GetOpponentSign());

        public Board.Point GetWonFromLeftUpDiagonalPoint(PlayerSign[,] fields, PlayerSign sign)
        {
            int signsCount = 0;
            int emptyFieldsCount = 0;

            for (int dimensionIndex = 0; dimensionIndex < 3; dimensionIndex++)
            {
                var currentField = fields[dimensionIndex, dimensionIndex];

                if (currentField == sign)
                    signsCount++;
                else if (currentField == PlayerSign.None)
                    emptyFieldsCount++;
            }

            if (signsCount == 2 && emptyFieldsCount == 1)
            {
                for (int index = 0; index < 3; index++)
                {
                    if (fields[index, index] == PlayerSign.None)
                        return new Board.Point(index, index);
                }
            }

            return null;
        }

        public Board.Point GetBlockFromLeftUpDiagonalPoint(PlayerSign[,] fields, PlayerSign sign) =>
            GetWonFromLeftUpDiagonalPoint(fields, sign.GetOpponentSign());

        public Board.Point GetWonFromRightUpDiagonalPoint(PlayerSign[,] fields, PlayerSign sign)
        {
            int signsCount = 0;
            int emptyFieldsCount = 0;

            for (int rowIndex = 0, columnIndex = 2; rowIndex < 3; rowIndex++, columnIndex--)
            {

                var currentField = fields[rowIndex, columnIndex];

                if (currentField == sign)
                    signsCount++;
                else if (currentField == PlayerSign.None)
                    emptyFieldsCount++;
            }

            if (signsCount == 2 && emptyFieldsCount == 1)
            {
                for (int rowIndex = 0, columnIndex = 2; rowIndex < 3; rowIndex++, columnIndex--)
                {
                    if (fields[rowIndex, columnIndex] == PlayerSign.None)
                        return new Board.Point(columnIndex, rowIndex);
                }
            }

            return null;
        }

        public Board.Point GetBlockFromRightUpDiagonalPoint(PlayerSign[,] fields, PlayerSign sign)
            => GetWonFromRightUpDiagonalPoint(fields, sign.GetOpponentSign());

    }
}
