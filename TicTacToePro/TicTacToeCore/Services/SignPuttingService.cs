﻿using TicTacToeCore.Enums;
using TicTacToeCore.Models;

namespace TicTacToeCore.Services
{
    public class SignPuttingService
    {
        public SignPuttingResult TryPutSign(Board board, PlayerSign signToPut, Board.Point fieldToPutSign)
        {
            SignPuttingResult result = new SignPuttingResult();

            if (board.Fields[fieldToPutSign.Y, fieldToPutSign.X] == PlayerSign.None)
            {
                board.Fields[fieldToPutSign.Y, fieldToPutSign.X] = signToPut;
                result.IsPut = true;
            }
            else
            {
                result.Message =
                    $"Pole jest zajęte, proszę wybrać inne.";
            }

            return result;
        }
    }
}
