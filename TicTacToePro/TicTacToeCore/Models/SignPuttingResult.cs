﻿namespace TicTacToeCore.Models
{
    public class SignPuttingResult
    {
        public bool IsPut { get; set; }
        public string Message { get; set; }
    }
}
