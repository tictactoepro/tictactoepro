﻿using System;
using System.Diagnostics;
using TicTacToeCore.Enums;
using TicTacToeCore.Helpers;

namespace TicTacToeCore.Models
{
    public class Board
    {
        /// <summary>
        /// Table that contains PlayerSign. Dimensions: [row=height(Y), column=width(X)].
        /// </summary>
        public PlayerSign[,] Fields { get; }

        private Board(PlayerSign[,] fields)
        {
            Fields = fields;
        }

        public int EmptyFieldsCount => BoardHelper.GetEmptyFieldsCount(this);

        //TODO: Find a better name.
        /// <summary>
        /// True if all fields contain X or O sign.
        /// </summary>
        public bool NoMovesLeft => EmptyFieldsCount == 0;

        public static Board GetBoard(PlayerSign[,] fields)
        {
            ValidateBoardData(fields);
            return new Board(fields);
        }

        private static void ValidateBoardData(PlayerSign[,] fields)
        {
            if (fields.GetLength(0) != 3 || fields.GetLength(1) != 3)
                throw new ArgumentException($"Board can be only 3x3 table. The given is: {fields.GetLength(0)}x{fields.GetLength(1)}.");
        }

        public override string ToString()
        {
            long yLength = Fields.GetLongLength(0);
            long xLength = Fields.GetLongLength(1);
            string result = String.Empty;

            for (int y = 0; y < yLength; y++)
            {
                for (int x = 0; x < xLength; x++)
                {
                    result += Fields[y, x] != PlayerSign.None ? Fields[y, x].ToString() : "[]";
                }

                result += Environment.NewLine;
            }

            return result;
        }

        [DebuggerDisplay("X:{X} Y:{Y}")]
        public class Point
        {
            private int _x;
            /// <summary>
            /// Column index in table creation (new T[y, x])
            /// </summary>
            public int X
            {
                get => _x;
                set
                {
                    ValidatePointData(value);
                    _x = value;
                }
            }

            private void ValidatePointData(int valueToSet)
            {
                int minValue = 0;
                int maxValue = 2;
                if (valueToSet < minValue || valueToSet > maxValue)
                    throw new ArgumentException($"Point values contain only values between {minValue} and {maxValue}. Value was {valueToSet}.");
            }
            private int _y;

            /// <summary>
            /// Row index in table creation (new T[y, x])
            /// </summary>
            public int Y
            {
                get => _y;
                set
                {
                    ValidatePointData(value);
                    _y = value;
                }
            }

            public Point()
            {

            }

            /// <summary>
            /// Ctor that requires X (column index) and Y (row index).
            /// </summary>
            /// <param name="x">Column index.</param>
            /// <param name="y">Row index.</param>
            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override bool Equals(object obj)
            {
                if(obj is Board.Point pointToCompare)
                    return X == pointToCompare.X && Y == pointToCompare.Y;

                return base.Equals(obj);
            }
        }
    }
}
