﻿using TicTacToeCore.Enums;

namespace TicTacToeCore.Models
{
    public class GameResult
    {
        public bool IsGameOver => ResultSign.HasValue;
        public PlayerSign? ResultSign { get; set; }

        public GameResult()
        {

        }

        public GameResult(PlayerSign? resultSign)
        {
            ResultSign = resultSign;
        }

        public override bool Equals(object obj)
        {
            if (obj is GameResult gameResult)
                return gameResult.ResultSign == ResultSign;

            return base.Equals(obj);
        }
    }
}
