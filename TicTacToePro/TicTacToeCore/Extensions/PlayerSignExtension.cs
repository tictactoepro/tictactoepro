﻿using System;
using TicTacToeCore.Enums;

namespace TicTacToeCore.Extensions
{
    public static class PlayerSignExtension
    {
        /// <summary>
        /// Returns opponent sign. For PlayerSign.None returns PlayerSign.None.
        /// </summary>
        /// <param name="sign"></param>
        /// <returns></returns>
        public static PlayerSign GetOpponentSign(this PlayerSign sign)
        {
            switch (sign)
            {
                case PlayerSign.X:
                    return PlayerSign.O;
                case PlayerSign.O:
                    return PlayerSign.X;
                case PlayerSign.None:
                    return PlayerSign.None;
                default:
                    throw new ArgumentException($"No implementation for {sign}.");
            }
        }
    }
}
