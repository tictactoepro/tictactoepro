﻿namespace TicTacToeCore.Enums
{
    public enum DimensionType
    {
        Rows,
        Columns,
        DiagonalLeftUp,
        DiagonalRightUp
    }
}
