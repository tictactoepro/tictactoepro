﻿namespace TicTacToeCore.Enums
{
    public enum PlayerSign
    {
        None,
        X,
        O
    }
}
