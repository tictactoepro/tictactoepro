﻿using System;
using System.Drawing;
using TicTacToeCore.Enums;
using TicTacToeUI.Properties;

namespace TicTacToeUI.Helpers
{
    public static class ResourcesHelper
    {
        public static Bitmap GetFieldBitmap(PlayerSign sign)
        {
            switch (sign)
            {
                case PlayerSign.None:
                    return Resources.EmptyField;
                case PlayerSign.O:
                    return Resources.Circle;
                case PlayerSign.X:
                    return Resources.Cross;
                default:
                    throw new ArgumentException($"No implementation for {sign}.");
            }
        }
    }
}
