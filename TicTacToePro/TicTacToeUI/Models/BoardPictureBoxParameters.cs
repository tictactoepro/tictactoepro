﻿using System;
using System.Windows.Forms;

namespace TicTacToeUI.Models
{
    public class BoardPictureBoxParameters
    {
        public Control.ControlCollection Controls { get; set; }
        public int RowIndex { get; set; }
        public int ColumnIndex { get; set; }
        public int LocationX { get; set; }
        public int LocationY { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int TabIndex { get; set; }
        public string Name { get; set; }
        public EventHandler ClickEvent { get; set; }

        public BoardPictureBoxParameters(Control.ControlCollection controls, int rowIndex, int columnIndex, int locationX, int locationY, int width, int height, int tabIndex, string name, EventHandler clickEvent)
        {
            Controls = controls;
            RowIndex = rowIndex;
            ColumnIndex = columnIndex;
            LocationX = locationX;
            LocationY = locationY;
            Width = width;
            Height = height;
            TabIndex = tabIndex;
            Name = name;
            ClickEvent = clickEvent;
        }
    }
}
