﻿using System;
using System.Windows.Forms;

namespace TicTacToeUI.Models
{
    public class BoardPictureBoxesPreparationParameters
    {
        public Control.ControlCollection Controls { get; set; }
        public PictureBox BoardBox { get; set; }
        public PictureBox[,] Fields { get; set; }
        public int BorderLineWidth { get; set; }
        public int FirstTabIndex { get; set; }
        public EventHandler ClickEvent { get; set; }

        public BoardPictureBoxesPreparationParameters(Control.ControlCollection controls, PictureBox boardBox, PictureBox[,] fields, int borderLineWidth, int firstTabIndex, EventHandler clickEvent)
        {
            Controls = controls;
            BoardBox = boardBox;
            Fields = fields;
            BorderLineWidth = borderLineWidth;
            FirstTabIndex = firstTabIndex;
            ClickEvent = clickEvent;
        }
    }
}
