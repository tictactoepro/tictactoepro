﻿using System.Windows.Forms;

namespace TicTacToeUI.Models
{
    public class BoardPictureBox : PictureBox
    {
        /// <summary>
        /// Box's row index on the board
        /// </summary>
        public int RowIndex { get; set; }
        /// <summary>
        /// Box's column index on the board
        /// </summary>
        public int ColumnIndex { get; set; }
    }
}
