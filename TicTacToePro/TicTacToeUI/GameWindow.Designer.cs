﻿using System.ComponentModel;
using System.Windows.Forms;

namespace TicTacToeUI
{
    partial class GameWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.boardBox = new System.Windows.Forms.PictureBox();
            this.currentMoveLabel = new System.Windows.Forms.Label();
            this.listBoxClientMessages = new System.Windows.Forms.ListBox();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.lblNewMessageTitle = new System.Windows.Forms.Label();
            this.txtNewMessage = new System.Windows.Forms.TextBox();
            this.lblChatHistoryTitle = new System.Windows.Forms.Label();
            this.txtChatHistory = new System.Windows.Forms.TextBox();
            this.btnStartClient = new System.Windows.Forms.Button();
            this.lblPortTitle = new System.Windows.Forms.Label();
            this.numericPort = new System.Windows.Forms.NumericUpDown();
            this.lblIPAddressTitle = new System.Windows.Forms.Label();
            this.comboIPAddress = new System.Windows.Forms.ComboBox();
            this.bwConnectionMaker = new System.ComponentModel.BackgroundWorker();
            this.bwConnectionHandler = new System.ComponentModel.BackgroundWorker();
            this.twoPlayersRadioButton = new System.Windows.Forms.RadioButton();
            this.onePlayerRadioButton = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.boardBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPort)).BeginInit();
            this.SuspendLayout();
            // 
            // boardBox
            // 
            this.boardBox.Image = global::TicTacToeUI.Properties.Resources.WhiteBackground;
            this.boardBox.Location = new System.Drawing.Point(26, 22);
            this.boardBox.MinimumSize = new System.Drawing.Size(512, 512);
            this.boardBox.Name = "boardBox";
            this.boardBox.Size = new System.Drawing.Size(512, 512);
            this.boardBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.boardBox.TabIndex = 0;
            this.boardBox.TabStop = false;
            // 
            // currentMoveLabel
            // 
            this.currentMoveLabel.AutoSize = true;
            this.currentMoveLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentMoveLabel.Location = new System.Drawing.Point(558, 47);
            this.currentMoveLabel.Name = "currentMoveLabel";
            this.currentMoveLabel.Size = new System.Drawing.Size(268, 48);
            this.currentMoveLabel.TabIndex = 1;
            this.currentMoveLabel.Text = "Obecny ruch:";
            // 
            // listBoxClientMessages
            // 
            this.listBoxClientMessages.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxClientMessages.FormattingEnabled = true;
            this.listBoxClientMessages.ItemHeight = 16;
            this.listBoxClientMessages.Location = new System.Drawing.Point(544, 184);
            this.listBoxClientMessages.Name = "listBoxClientMessages";
            this.listBoxClientMessages.Size = new System.Drawing.Size(688, 100);
            this.listBoxClientMessages.TabIndex = 14;
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendMessage.Location = new System.Drawing.Point(1130, 621);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(101, 30);
            this.btnSendMessage.TabIndex = 33;
            this.btnSendMessage.Text = "Wyślij";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.BtnSendMessage_Click);
            // 
            // lblNewMessageTitle
            // 
            this.lblNewMessageTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNewMessageTitle.AutoSize = true;
            this.lblNewMessageTitle.Location = new System.Drawing.Point(541, 513);
            this.lblNewMessageTitle.Name = "lblNewMessageTitle";
            this.lblNewMessageTitle.Size = new System.Drawing.Size(120, 17);
            this.lblNewMessageTitle.TabIndex = 32;
            this.lblNewMessageTitle.Text = "Nowa wiadomość:";
            // 
            // txtNewMessage
            // 
            this.txtNewMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewMessage.Location = new System.Drawing.Point(541, 542);
            this.txtNewMessage.Multiline = true;
            this.txtNewMessage.Name = "txtNewMessage";
            this.txtNewMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNewMessage.ShortcutsEnabled = false;
            this.txtNewMessage.Size = new System.Drawing.Size(690, 73);
            this.txtNewMessage.TabIndex = 31;
            this.txtNewMessage.TextChanged += new System.EventHandler(this.txtNewMessage_TextChanged);
            this.txtNewMessage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNewMessage_KeyPress);
            // 
            // lblChatHistoryTitle
            // 
            this.lblChatHistoryTitle.AutoSize = true;
            this.lblChatHistoryTitle.Location = new System.Drawing.Point(541, 343);
            this.lblChatHistoryTitle.Name = "lblChatHistoryTitle";
            this.lblChatHistoryTitle.Size = new System.Drawing.Size(137, 17);
            this.lblChatHistoryTitle.TabIndex = 30;
            this.lblChatHistoryTitle.Text = "Historia konwersacji:";
            // 
            // txtChatHistory
            // 
            this.txtChatHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChatHistory.Location = new System.Drawing.Point(544, 372);
            this.txtChatHistory.Multiline = true;
            this.txtChatHistory.Name = "txtChatHistory";
            this.txtChatHistory.ReadOnly = true;
            this.txtChatHistory.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtChatHistory.ShortcutsEnabled = false;
            this.txtChatHistory.Size = new System.Drawing.Size(688, 126);
            this.txtChatHistory.TabIndex = 29;
            // 
            // btnStartClient
            // 
            this.btnStartClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStartClient.Location = new System.Drawing.Point(1156, 302);
            this.btnStartClient.Name = "btnStartClient";
            this.btnStartClient.Size = new System.Drawing.Size(75, 23);
            this.btnStartClient.TabIndex = 28;
            this.btnStartClient.Text = "Start";
            this.btnStartClient.UseVisualStyleBackColor = true;
            this.btnStartClient.Click += new System.EventHandler(this.BtnStartClient_Click);
            // 
            // lblPortTitle
            // 
            this.lblPortTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPortTitle.AutoSize = true;
            this.lblPortTitle.Location = new System.Drawing.Point(1007, 303);
            this.lblPortTitle.Name = "lblPortTitle";
            this.lblPortTitle.Size = new System.Drawing.Size(38, 17);
            this.lblPortTitle.TabIndex = 27;
            this.lblPortTitle.Text = "Port:";
            // 
            // numericPort
            // 
            this.numericPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numericPort.Location = new System.Drawing.Point(1051, 301);
            this.numericPort.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numericPort.Name = "numericPort";
            this.numericPort.Size = new System.Drawing.Size(82, 22);
            this.numericPort.TabIndex = 26;
            this.numericPort.Value = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            // 
            // lblIPAddressTitle
            // 
            this.lblIPAddressTitle.AutoSize = true;
            this.lblIPAddressTitle.Location = new System.Drawing.Point(541, 303);
            this.lblIPAddressTitle.Name = "lblIPAddressTitle";
            this.lblIPAddressTitle.Size = new System.Drawing.Size(65, 17);
            this.lblIPAddressTitle.TabIndex = 25;
            this.lblIPAddressTitle.Text = "Adres IP:";
            // 
            // comboIPAddress
            // 
            this.comboIPAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboIPAddress.FormattingEnabled = true;
            this.comboIPAddress.Location = new System.Drawing.Point(622, 300);
            this.comboIPAddress.Name = "comboIPAddress";
            this.comboIPAddress.Size = new System.Drawing.Size(343, 24);
            this.comboIPAddress.TabIndex = 24;
            this.comboIPAddress.Text = "127.0.0.1";
            this.comboIPAddress.TextChanged += new System.EventHandler(this.ComboIPAddress_TextChanged);
            // 
            // bwConnectionMaker
            // 
            this.bwConnectionMaker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwConnectionMaker_DoWork);
            // 
            // bwConnectionHandler
            // 
            this.bwConnectionHandler.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwConnectionHandler_DoWork);
            // 
            // twoPlayersRadioButton
            // 
            this.twoPlayersRadioButton.AutoSize = true;
            this.twoPlayersRadioButton.Checked = true;
            this.twoPlayersRadioButton.Location = new System.Drawing.Point(566, 22);
            this.twoPlayersRadioButton.Name = "twoPlayersRadioButton";
            this.twoPlayersRadioButton.Size = new System.Drawing.Size(83, 21);
            this.twoPlayersRadioButton.TabIndex = 34;
            this.twoPlayersRadioButton.TabStop = true;
            this.twoPlayersRadioButton.Text = "2 graczy";
            this.twoPlayersRadioButton.UseVisualStyleBackColor = true;
            this.twoPlayersRadioButton.CheckedChanged += new System.EventHandler(this.twoPlayersRadioButton_CheckedChanged);
            // 
            // onePlayerRadioButton
            // 
            this.onePlayerRadioButton.AutoSize = true;
            this.onePlayerRadioButton.Location = new System.Drawing.Point(655, 22);
            this.onePlayerRadioButton.Name = "onePlayerRadioButton";
            this.onePlayerRadioButton.Size = new System.Drawing.Size(41, 21);
            this.onePlayerRadioButton.TabIndex = 35;
            this.onePlayerRadioButton.Text = "AI";
            this.onePlayerRadioButton.UseVisualStyleBackColor = true;
            this.onePlayerRadioButton.CheckedChanged += new System.EventHandler(this.onePlayerRadioButton_CheckedChanged);
            // 
            // GameWindow
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1262, 721);
            this.Controls.Add(this.onePlayerRadioButton);
            this.Controls.Add(this.twoPlayersRadioButton);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.lblNewMessageTitle);
            this.Controls.Add(this.txtNewMessage);
            this.Controls.Add(this.lblChatHistoryTitle);
            this.Controls.Add(this.txtChatHistory);
            this.Controls.Add(this.btnStartClient);
            this.Controls.Add(this.lblPortTitle);
            this.Controls.Add(this.numericPort);
            this.Controls.Add(this.lblIPAddressTitle);
            this.Controls.Add(this.comboIPAddress);
            this.Controls.Add(this.listBoxClientMessages);
            this.Controls.Add(this.currentMoveLabel);
            this.Controls.Add(this.boardBox);
            this.MaximumSize = new System.Drawing.Size(1280, 768);
            this.MinimumSize = new System.Drawing.Size(1280, 768);
            this.Name = "GameWindow";
            this.Text = "TicTacChat";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GameWindow_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.boardBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PictureBox boardBox;
        private Label currentMoveLabel;
        private ListBox listBoxClientMessages;
        private Button btnSendMessage;
        private Label lblNewMessageTitle;
        private TextBox txtNewMessage;
        private Label lblChatHistoryTitle;
        private TextBox txtChatHistory;
        private Button btnStartClient;
        private Label lblPortTitle;
        private NumericUpDown numericPort;
        private Label lblIPAddressTitle;
        private ComboBox comboIPAddress;
        private BackgroundWorker bwConnectionMaker;
        private BackgroundWorker bwConnectionHandler;
        private RadioButton twoPlayersRadioButton;
        private RadioButton onePlayerRadioButton;
    }
}

