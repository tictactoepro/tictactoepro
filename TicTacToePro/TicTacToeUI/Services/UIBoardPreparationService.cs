﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TicTacToeUI.Models;

namespace TicTacToeUI.Services
{
    public class UIBoardPreparationService
    {
        public void PrepareBoardPictureBoxes(BoardPictureBoxesPreparationParameters parameters)
        {
            int fieldX = parameters.BoardBox.Location.X;
            int fieldY = parameters.BoardBox.Location.Y;
            var widthMinusBorderLines = parameters.BoardBox.Width - parameters.BorderLineWidth * 2;
            int fieldWidth = widthMinusBorderLines / 3;
            int fieldHeight = fieldWidth;
            int tabIndex = parameters.FirstTabIndex;

            for (int rowIndex = 0; rowIndex < 3; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < 3; columnIndex++)
                {
                    parameters.Fields[rowIndex, columnIndex] = AddPictureBox(
                        new BoardPictureBoxParameters(parameters.Controls, rowIndex, columnIndex, fieldX, fieldY,
                            fieldWidth, fieldHeight, tabIndex++, $"field{rowIndex}{columnIndex}",
                            parameters.ClickEvent));

                    fieldX += fieldWidth + parameters.BorderLineWidth;
                }

                fieldX = parameters.BoardBox.Location.X;
                fieldY += fieldHeight + parameters.BorderLineWidth;
            }
        }

        public BoardPictureBox AddPictureBox(BoardPictureBoxParameters parameters)
        {
            var pictureBox = new BoardPictureBox();
            parameters.Controls.Add(pictureBox);
            ((ISupportInitialize)(pictureBox)).BeginInit();

            pictureBox.RowIndex = parameters.RowIndex;
            pictureBox.ColumnIndex = parameters.ColumnIndex;
            pictureBox.Location = new Point(parameters.LocationX, parameters.LocationY);
            pictureBox.Name = parameters.Name;
            pictureBox.Size = new Size(parameters.Width, parameters.Height);
            pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox.TabIndex = parameters.TabIndex;
            pictureBox.TabStop = false;
            pictureBox.Visible = true;
            pictureBox.BringToFront();
            pictureBox.Click += parameters.ClickEvent;

            ((ISupportInitialize)(pictureBox)).EndInit();

            return pictureBox;
        }
    }
}
