﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using TicTacToeCore.Enums;
using TicTacToeCore.Extensions;
using TicTacToeCore.Helpers;
using TicTacToeCore.Models;
using TicTacToeCore.Services;
using TicTacToeUI.Enums;
using TicTacToeUI.Helpers;
using TicTacToeUI.Models;
using TicTacToeUI.Services;

namespace TicTacToeUI
{
    public partial class GameWindow : Form
    {
        #region GameLogicAndInitialization

        private readonly SignPuttingService _signPuttingService = new SignPuttingService();
        private readonly UIBoardPreparationService _boardPreparationService = new UIBoardPreparationService();
        private readonly BoardPictureBox[,] _boardFieldsPictureBoxes = new BoardPictureBox[3, 3];
        private readonly AIMovesService _aIMovesService = new AIMovesService();
        private Board _board;
        private PlayerSign _signToPut;
        private GameMode _gameMode;

        public GameWindow()
        {
            InitializeComponent();
            FillIpAddressesCombo();
            _boardPreparationService.PrepareBoardPictureBoxes(new BoardPictureBoxesPreparationParameters(
                Controls, boardBox, _boardFieldsPictureBoxes, 4, 1, BoardPictureBox_Click));
            PrepareGame();
        }

        private void PrepareGame()
        {
            UpdateGameMode();

            _board = BoardHelper.GetEmptyBoard();
            LoadFieldsImages();

            _signToPut = PlayerSign.X;
            FillCurrentMoveLabel();
        }

        private void UpdateGameMode() => _gameMode = GetGameMode();

        private GameMode GetGameMode() => onePlayerRadioButton.Checked ? GameMode.AI : GameMode.HotSeat;

        private void LoadFieldsImages()
        {
            for (int rowIndex = 0; rowIndex < 3; rowIndex++)
            {
                for (int columnIndex = 0; columnIndex < 3; columnIndex++)
                {
                    _boardFieldsPictureBoxes[rowIndex, columnIndex].Image =
                        ResourcesHelper.GetFieldBitmap(_board.Fields[rowIndex, columnIndex]);
                }
            }
        }

        private void FillCurrentMoveLabel()
        {
            currentMoveLabel.Text = "Obecny ruch:" + _signToPut;
        }

        private void BoardPictureBox_Click(object sender, EventArgs e)
        {
            BoardPictureBox boardPictureBox = (BoardPictureBox)(sender);
            Board.Point pointToPutSign = new Board.Point(boardPictureBox.ColumnIndex, boardPictureBox.RowIndex);

            PutSign(pointToPutSign);

        }

        private void PutSign(Board.Point pointToPutSign)
        {
            var puttingResult = _signPuttingService.TryPutSign(_board, _signToPut, pointToPutSign);

            if (puttingResult.IsPut)
            {
                UpdateDataAfterMove(pointToPutSign);
            }
            else
                MessageBox.Show(puttingResult.Message);
        }

        private bool IsAIMove => _gameMode == GameMode.AI && _signToPut == PlayerSign.O;

        private void UpdateDataAfterMove(Board.Point pointWithNewSign)
        {
            _boardFieldsPictureBoxes[pointWithNewSign.Y, pointWithNewSign.X].Image = ResourcesHelper.GetFieldBitmap(_signToPut);
            _signToPut = _signToPut.GetOpponentSign();
            LoadFieldsImages();

            GameResult gameResult = GetGameResult();
            if (gameResult.IsGameOver)
            {
                HandleOverGame(gameResult);
            }
            else
            {
                if (IsAIMove)
                {
                    PutSign(_aIMovesService.GetNextMovePoint(_board.Fields, _signToPut));
                }
                else
                {
                    FillCurrentMoveLabel();
                }
            }

        }

        private GameResult GetGameResult()
        {
            GameResultService gameResultService = new GameResultService();
            return gameResultService.CheckGameResult(_board);
        }

        private void HandleOverGame(GameResult gameResult)
        {
            var dialogResult = MessageBox.Show(
                 GetResultText(gameResult.ResultSign.Value) + Environment.NewLine + Environment.NewLine +
                 "Czy chcesz zagrać ponownie?", "Koniec gry", MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
                PrepareGame();
        }

        private string GetResultText(PlayerSign resultSign)
        {
            if (resultSign == PlayerSign.None)
                return "Remis!";

            return $"{resultSign} zwycięża!";
        }


        private void onePlayerRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            HandleModeChange((RadioButton)sender, twoPlayersRadioButton);
        }

        private void twoPlayersRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            HandleModeChange((RadioButton)sender, onePlayerRadioButton);
        }

        private bool _ignoreNextChange;

        private void HandleModeChange(RadioButton changedRadioButton, RadioButton secondRadioButton)
        {
            if (_ignoreNextChange)
            {
                _ignoreNextChange = false;
                return;
            }

            //If mode isn't changed we do not make any action.
            if (changedRadioButton.Checked == false)
                return;

            if (_board.EmptyFieldsCount < 9)
            {
                var userChoose =
                    MessageBox.Show("Po zmianie tej opcji obecna gra zostanie przerwana. Czy na pewno chcesz zmienić tryb gry?",
                        "Zmiana trybu", MessageBoxButtons.YesNo);

                if (userChoose == DialogResult.Yes)
                {
                    //secondRadioButton.Checked = false;
                    PrepareGame();
                }
                else
                {
                    changedRadioButton.Checked = false;
                    _ignoreNextChange = true;
                    secondRadioButton.Checked = true;
                }
            }
            else
            {
                UpdateGameMode();
            }
        }

        #endregion

        #region ChatClient

        //stala z domyslnym adresem IP (lokalny adres)
        private const string DefaultIpAddress = "127.0.0.1";

        //stale z początkowym i koncowym łańcuchem znaków
        private const string InviteCode = "###START###";
        private const string ByeByeCode = "###KONIEC###";

        //klient polaczenia
        private TcpClient _tcpClient;

        //adres IP do połączenia
        private string _ipAddress = DefaultIpAddress;

        //odczyt danych
        private BinaryReader _reader;

        //wysylka danych
        private BinaryWriter _writer;

        //czy polaczenie aktywne
        private bool _isConnectionActive;

        private void FillIpAddressesCombo()
        {
            //wypelnienie listy adresow IP danymi pobranymi z naszego komputera
            IPHostEntry ipHostEntry = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ipAddress in ipHostEntry.AddressList)
            {
                comboIPAddress.Items.Add(ipAddress.ToString());
            }
        }

        //przykład delegatu
        delegate void SetTextCallBack(string text);

        //wykorzystanie delegatu dla kontrolki z historia komunikatów klienta
        private void SetClientMessagesText(string text)
        {
            if (listBoxClientMessages.InvokeRequired)
            {
                SetTextCallBack setTextCallback = SetClientMessagesText;
                Invoke(setTextCallback, text);
            }
            else
            {
                listBoxClientMessages.Items.Add(text);
            }
        }

        //wykorzystanie delegatu dla kontrolki z historią wiadomości
        private void SetChatHistoryText(string text)
        {
            if (txtChatHistory.InvokeRequired)
            {
                SetTextCallBack setTextCallback = SetChatHistoryText;
                Invoke(setTextCallback, text);
            }
            else
            {
                txtChatHistory.AppendText(text);
            }
        }

        //wpisanie nowej wiadomości do historii chatu z wykorzystaniem metod powyżej
        private void PutMessageToChatHistory(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                // dla pustego tekstu lub białych znaków nic nie robimy
                return;
            }
            //znak nowej linii
            if (txtChatHistory.Text != string.Empty)
                SetChatHistoryText(Environment.NewLine);
            //wpisanie tekstu w kontrolkę, wyciecie bialych znakow na poczatku i koncu
            SetChatHistoryText(text.Trim());
        }

        private void BtnStartClient_Click(object sender, EventArgs e)
        {
            HandleConnectionStateChange();   
        }

        private void HandleConnectionStateChange()
        {
            try
            {
                //jezeli polaczenie nie jest aktywne
                if (!_isConnectionActive)
                {
                    //oznaczenie polaczenia jako aktywnego
                    _isConnectionActive = true;
                    //uruchom watek odpowiedzialny za połączenie
                    bwConnectionMaker.RunWorkerAsync();
                    //zmiana etykiety przycisku
                    btnStartClient.Text = "Stop";
                }
                //jezeli polaczenie jest aktywne
                else
                {
                    //oznaczenie polaczenia jako nieaktywnego
                    _isConnectionActive = false;
                    if (_tcpClient != null)
                    {
                        //wyslanie komunikatu pozegnalnego
                        _writer.Write(ByeByeCode);
                        //zatrzymanie klienta tcp
                        _tcpClient.Close();
                    }
                    if (bwConnectionMaker.IsBusy)
                    {
                        //zatrzymanie watku odpowiedzialnego za nawiazanie połączenia
                        bwConnectionMaker.CancelAsync();
                    }
                    if (bwConnectionHandler.IsBusy)
                    {
                        //zatrzymanie watku odpowiedzialnego za wiadomości
                        bwConnectionHandler.CancelAsync();
                    }
                    //zmiana etykiety przycisku
                    btnStartClient.Text = "Start";
                }
            }
            catch (Exception ex)
            {
                //obsługa wyjątku - wyświetlamy komunikat
                SetClientMessagesText("Wystąpił błąd...");
                SetClientMessagesText($"Komunikat błędu: {ex.Message}");
                btnStartClient.Text = "Start";
            }
        }

        private void BtnSendMessage_Click(object sender, EventArgs e)
        {
            //jezeli polaczenie jest aktywne - wysylamy wiadomosc
            if (_isConnectionActive)
            {
                //wysylka wiadomosci do strumienia Klienta
                _writer.Write(txtNewMessage.Text);
                //wpisanie wiadomosci w historie konwersacji
                PutMessageToChatHistory($"Klient: {txtNewMessage.Text}");
                //wyczyszczenie pola do wysylki wiadomosci
                txtNewMessage.Clear();
            }
        }

        private void txtNewMessage_KeyPress(object sender, KeyPressEventArgs e)
        {
            //wysylka wiadomosci tez na enter
            if (e.KeyChar == (char)Keys.Enter)
            {
                BtnSendMessage_Click(sender, e);
                txtNewMessage.Text = String.Empty;;
            }
        }

        private void ComboIPAddress_TextChanged(object sender, EventArgs e)
        {

        }

        private void bwConnectionMaker_DoWork(object sender, DoWorkEventArgs e)
        {
            IPAddress serverCurrentIpAddress;
            try
            {
                //weryfikacja adresu IP do nasłuchu
                serverCurrentIpAddress = IPAddress.Parse(_ipAddress);
            }
            catch
            {
                //w przypadku weryfikacji negatywnej (wystąpienie wyjątku) wyświetlamy stosowny komunikat i kończymy połączenie
                MessageBox.Show("Błędny adres IP...");
                _isConnectionActive = false;
                return;
            }
            try
            {
                //wyświetlenie komunikatu o probie nawiazanie polaczenia
                SetClientMessagesText("Nawiązywanie połączenia...");
                //proba nawiazania połączenia na skonfigurowanym adresie i porcie
                _tcpClient = new TcpClient(_ipAddress, (int)numericPort.Value);
                //strumień danych połączenia
                NetworkStream networkStream = _tcpClient.GetStream();
                //spięcie zapisu i odczytu ze strumieniem danych z połączenia
                _reader = new BinaryReader(networkStream);
                _writer = new BinaryWriter(networkStream);
                //wyświetlenie komunikatu o probie uwierzytelnienia
                SetClientMessagesText("Uwierzytelnianie...");
                //wysłanie powitalnego kodu na serwer (proba uwierzytelnienia)
                _writer.Write(InviteCode);
                //w przypadku udanego uwierzytelnienia uruchomienie wątku odpowiedzialnego za obsługę wiadomości
                _isConnectionActive = true;
                bwConnectionHandler.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                //obsługa wyjątku - wyświetlamy komunikat i dezaktywujemy połączenie
                SetClientMessagesText("Wystąpił błąd... Połączenie zostało przerwane.");
                SetClientMessagesText($"Komunikat błędu: {ex.Message}");
                _isConnectionActive = false;
                btnStartClient.Text = "Start";
            }
        }

        private void bwConnectionHandler_DoWork(object sender, DoWorkEventArgs e)
        {
            //wyświetlenie komunikatu o udanym uwierzytelnieniu
            SetClientMessagesText("Uwierzytelnianie powiodło się...");
            //deklaracja zmiennej
            string message;
            try
            {
                //dopoki przychodzaca wiadomosc od Klienta nie jest pozegnaniem
                while (!(message = _reader.ReadString()).Equals(ByeByeCode))
                {
                    //dopisuj wiadomości do okienka konwersacji
                    PutMessageToChatHistory($"Serwer: {message}");
                }
                //wyswietlanie komunikatu o pozegnaniu
                SetClientMessagesText("Połączenie zakończone.");
            }
            catch (Exception ex)
            {
                //obsługa wyjątku - wyświetlamy komunikat i dezaktywujemy połączenie
                SetClientMessagesText("Wystąpił błąd... Połączenie zostało przerwane.");
                SetClientMessagesText($"Komunikat błędu: {ex.Message}");
                btnStartClient.Text = "Start";
            }
            finally
            {
                //blok finally - wykona się zawsze na końcu
                _isConnectionActive = false;
                _tcpClient.Close();
            }
        }

        #endregion

        private void txtNewMessage_TextChanged(object sender, EventArgs e)
        {
            if (txtNewMessage.Text == Environment.NewLine)
            {
                txtNewMessage.Clear();
            }
        }

        private void GameWindow_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}