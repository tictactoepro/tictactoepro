﻿using NUnit.Framework;
using TicTacToeCore.Enums;
using TicTacToeCore.Extensions;

namespace TicTacToeCoreTests.Extensions
{
    [TestFixture]
    public class PlayerSignExtensionTests
    {
        [TestCase(PlayerSign.X, PlayerSign.O)]
        [TestCase(PlayerSign.O, PlayerSign.X)]
        [TestCase(PlayerSign.None, PlayerSign.None)]//For PlayerSign.None there is no opponent so we just want to receive PlayerSign.None
        public void GetOpponentSign_Sign_OpponentsSign(PlayerSign expectedOpponentsSign, PlayerSign sign)
        {
            PlayerSign opponentSign = sign.GetOpponentSign();
            Assert.AreEqual(expectedOpponentsSign, opponentSign);
        }
    }
}
