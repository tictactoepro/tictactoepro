﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using TicTacToeCore.Enums;
using TicTacToeCore.Extensions;
using TicTacToeCore.Helpers;
using TicTacToeCore.Models;
using TicTacToeCore.Services;

namespace TicTacToeCoreTests.Services
{
    [TestFixture]
    public class AIMovesServiceTests
    {
        private static PlayerSign Empty = PlayerSign.None;
        private static PlayerSign X = PlayerSign.X;
        private static PlayerSign O = PlayerSign.O;

        [Test]
        public void GetNextMovePoint_AllTestTypesForWin_WinningPoint()
        {
            List<(DimensionType type, Exception exception)> failedAssertions = new List<(DimensionType type, Exception exception)>();
            foreach (var enumValue in (DimensionType[])Enum.GetValues(typeof(DimensionType)))
            {
                try
                {
                    InitializeTestsLoop(enumValue,
                        (fields, sign) => GetAIMovesService().GetNextMovePoint(fields, sign));
                }
                catch (Exception ex)
                {
                   failedAssertions.Add((enumValue, ex));
                }
            }

            Assert.IsTrue(failedAssertions.Count == 0, GetAllTestTypesFailedAssertionMessage(failedAssertions));
        }

        private string GetAllTestTypesFailedAssertionMessage(List<(DimensionType type, Exception exception)> failedAssertions)
        {
            return $"For {failedAssertions.Count} enum values occured an error:" +
                   Environment.NewLine + string.Join(Environment.NewLine + Environment.NewLine,
                       failedAssertions.Select(assertion =>
                           $"For \"{assertion.type.GetType().Name}.{assertion.type}\": " + Environment.NewLine + assertion.exception));
        }

        [Test]
        public void GetWonInRowPoint_PossibilityToWinInRow_WinningPoint()
        {
            InitializeTestsLoop(DimensionType.Rows, (fields, sign) => GetAIMovesService().GetWonInRowPoint(fields, sign));
        }

        [Test]
        public void GetBlockInRowPoint_PossibilityToBlockInRow_BlockingPoint()
        {
            InitializeTestsLoop(DimensionType.Rows, (fields, sign) => 
                GetAIMovesService().GetBlockInRowPoint(fields, sign.GetOpponentSign()));
        }

        [Test]
        public void GetWonInColumnPoint_PossibilityToWinInColumn_WinningPoint()
        {
            InitializeTestsLoop(DimensionType.Columns, (fields, sign) => GetAIMovesService().GetWonInColumnPoint(fields, sign));
        }

        [Test]
        public void GetBlockInColumnPoint_PossibilityToBlockInColumn_BlockingPoint()
        {
            InitializeTestsLoop(DimensionType.Columns, (fields, sign) =>
                GetAIMovesService().GetBlockInColumnPoint(fields, sign.GetOpponentSign()));
        }

        [Test]
        public void GetWonFromLeftUpDiagonalPoint_PossibilityToWinFromLeftUpDiagonal_WinningPoint()
        {
            InitializeTestsLoop(DimensionType.DiagonalLeftUp, (fields, sign) => GetAIMovesService().GetWonFromLeftUpDiagonalPoint(fields, sign));
        }

        [Test]
        public void GetBlockFromLeftUpDiagonalPoint_PossibilityToBlockFromLeftUpDiagonal_BlockingPoint()
        {
            InitializeTestsLoop(DimensionType.DiagonalLeftUp, (fields, sign) =>
                GetAIMovesService().GetBlockFromLeftUpDiagonalPoint(fields, sign.GetOpponentSign()));
        }

        [Test]
        public void GetWonFromRightUpDiagonalPoint_PossibilityToWinFromRightUpDiagonal_WinningPoint()
        {
            InitializeTestsLoop(DimensionType.DiagonalRightUp, (fields, sign) => GetAIMovesService().GetWonFromRightUpDiagonalPoint(fields, sign));
        }

        [Test]
        public void GetBlockFromRightUpDiagonalPoint_PossibilityToBlockFromRightUpDiagonal_BlockingPoint()
        {
            InitializeTestsLoop(DimensionType.DiagonalRightUp, (fields, sign) =>
                GetAIMovesService().GetBlockFromRightUpDiagonalPoint(fields, sign.GetOpponentSign()));
        }

        public void InitializeTestsLoop(DimensionType complexTestType, Func<PlayerSign[,], PlayerSign, Board.Point> getMovePoint)
        {
            PlayerSign aiSign = Empty;

            for (int aiSignId = 1; aiSignId < 3; aiSignId++)
            {
                aiSign = (PlayerSign)aiSignId;
                BoardTestCasesProcessing(complexTestType, aiSign, getMovePoint);
            }
        }

        public void BoardTestCasesProcessing(DimensionType complexTestType, PlayerSign aiSign, Func<PlayerSign[,], PlayerSign, Board.Point> getMovePoint)
        {
            for (int firstDimensionToTestIndex = 0; firstDimensionToTestIndex < 3; firstDimensionToTestIndex++)
            {
                for (int secondDimensionToTestIndex = 0; secondDimensionToTestIndex <
                    GetSecondDimensionIndexesCount(complexTestType); secondDimensionToTestIndex++)
                {
                    Board board = BoardHelper.GetEmptyBoard();

                    for (int secondDimensionToFillIndex = 0; 
                        secondDimensionToFillIndex < 3; secondDimensionToFillIndex++)
                    {
                        if (secondDimensionToFillIndex != secondDimensionToTestIndex)
                        {
                            Board.Point point = PointByDimensionTypeHelper.GetPoint(complexTestType, firstDimensionToTestIndex, secondDimensionToFillIndex);
                            board.Fields[point.Y, point.X] = aiSign;
                        }
                    }

                    Board.Point expectedPoint = PointByDimensionTypeHelper.GetPoint(complexTestType, firstDimensionToTestIndex, secondDimensionToTestIndex);
                    Board.Point receivedPoint = getMovePoint(board.Fields, aiSign);
                    Assert.AreEqual(expectedPoint, receivedPoint, GetFailedAssertionMessage(complexTestType, aiSign, expectedPoint, receivedPoint));
                }
            }
        }

        /// <summary>
        /// Returns indexes count for second tests dimension - for diagonals we don't need second dimension's iteration.
        /// </summary>
        private int GetSecondDimensionIndexesCount(DimensionType dimensionType) =>
            dimensionType == DimensionType.DiagonalLeftUp || dimensionType == DimensionType.DiagonalRightUp ? 1 : 3;

        private string GetFailedAssertionMessage(DimensionType complexTestType,
            PlayerSign aiSign, Board.Point expectedPoint, Board.Point receivedPoint) =>
            $"Failed for complexTestType:\"{complexTestType}\" and aiSign:\"{aiSign}\"." + Environment.NewLine +
            $"Expected point row(Y):{expectedPoint.Y}, column(X):{expectedPoint.X}." + Environment.NewLine +
            $"Were row(Y):{receivedPoint.Y} and column(X):{receivedPoint.X}.";
        
        private AIMovesService GetAIMovesService() => new AIMovesService();
    }
}
