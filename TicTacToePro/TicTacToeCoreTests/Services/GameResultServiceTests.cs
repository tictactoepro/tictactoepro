﻿using NUnit.Framework;
using TicTacToeCore.Enums;
using TicTacToeCore.Helpers;
using TicTacToeCore.Models;
using TicTacToeCore.Services;

namespace TicTacToeCoreTests.Services
{
    [TestFixture]
    public class GameResultServiceTests
    {
        private static PlayerSign Empty = PlayerSign.None;
        private static PlayerSign X = PlayerSign.X;
        private static PlayerSign O = PlayerSign.O;

        [Test]
        public void CheckGameResult_XWonInFirstRow_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, X, X }, { O, Empty, O }, { Empty, Empty, Empty } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonInSecondRow_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, Empty, O }, { X, X, X }, { Empty, Empty, Empty } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonInThirdRow_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, Empty, O }, { Empty, Empty, Empty }, { X, X, X } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonInFirstRow_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, O, O }, { X, Empty, X }, { Empty, Empty, Empty } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonInSecondRow_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, Empty, X }, { O, O, O }, { Empty, Empty, Empty } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonInThirdRow_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, Empty, X }, { Empty, Empty, Empty }, { O, O, O } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonInFirstColumn_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, O, Empty }, { X, Empty, Empty }, { X, O, Empty } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonInSecondColumn_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, X, Empty }, { Empty, X, Empty }, { O, X, Empty } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonInThirdColumn_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, Empty, X }, { Empty, Empty, X }, { O, Empty, X } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonInFirstColumn_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, X, Empty }, { O, Empty, Empty }, { O, X, Empty } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonInSecondColumn_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, O, Empty }, { Empty, O, Empty }, { X, O, Empty } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonInThirdColumn_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, Empty, O }, { Empty, Empty, O }, { X, Empty, O } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonFromLeftUpDiagonal_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, Empty, O }, { Empty, X, Empty }, { O, Empty, X } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonFromRightUpDiagonal_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, Empty, X }, { Empty, X, O }, { X, Empty, Empty } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonFromLeftUpDiagonal_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { O, Empty, X }, { X, O, Empty }, { X, Empty, O } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_OWonFromRightUpDiagonal_OAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, Empty, O }, { X, O, X }, { O, Empty, Empty } });
            GameResult gameResult = new GameResult(O);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_XWonWholeBoardIsFilled_XAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, O, O }, { X, O, X }, { X, X, O } });
            GameResult gameResult = new GameResult(X);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_Draw_EmptyAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, O, X }, { O, X, X }, { O, X, O } });
            GameResult gameResult = new GameResult(Empty);
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        [Test]
        public void CheckGameResult_GameNotFinished1_NullAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, O, X }, { O, X, X }, { Empty, Empty, Empty } });
            GameResult gameResult = new GameResult();
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }
        [Test]
        public void CheckGameResult_GameNotFinished2_NullAsResultSign()
        {
            Board board = BoardHelper.GetFilledBoard(new[,] { { X, O, X }, { O, Empty, X }, { X, Empty, O } });
            GameResult gameResult = new GameResult();
            CheckGameResult_Board_CorrectGameResult(board, gameResult);
        }

        public void CheckGameResult_Board_CorrectGameResult(Board board, GameResult expectedGameResult)
        {
            GameResult gameResult = GetGameResultService().CheckGameResult(board);

            Assert.AreEqual(expectedGameResult, gameResult);
        }

        private GameResultService GetGameResultService() => new GameResultService();
    }
}
