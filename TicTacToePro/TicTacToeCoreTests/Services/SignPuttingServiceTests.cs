﻿using NUnit.Framework;
using TicTacToeCore.Enums;
using TicTacToeCore.Helpers;
using TicTacToeCore.Models;
using TicTacToeCore.Services;

namespace TicTacToeCoreTests.Services
{
    [TestFixture]
    public class SignPuttingServiceTests
    {
        [Test]
        public void TryPutSign_DifferentSignOnOccupiedField_NotPutAsResult()
        {
            Board board = BoardHelper.GetEmptyBoard();
            board.Fields[0, 0] = PlayerSign.X;
            PlayerSign signToPut = PlayerSign.O;
            Board.Point fieldToPutSign = new Board.Point(0, 0);

            SignPuttingResult puttingResult = GetPuttingService().TryPutSign(board, signToPut, fieldToPutSign);

            Assert.IsFalse(puttingResult.IsPut);
        }

        [Test]
        public void TryPutSign_TheSameSignOnOccupiedField_NotPutAsResult()
        {
            Board board = BoardHelper.GetEmptyBoard();
            board.Fields[0, 0] = PlayerSign.X;
            PlayerSign signToPut = PlayerSign.X;
            Board.Point fieldToPutSign = new Board.Point(0, 0);

            SignPuttingResult puttingResult = GetPuttingService().TryPutSign(board, signToPut, fieldToPutSign);

            Assert.IsFalse(puttingResult.IsPut);
        }

        [Test]
        public void TryPutSign_SignOnEmptyField_PutAsResult()
        {
            Board board = BoardHelper.GetEmptyBoard();
            PlayerSign signToPut = PlayerSign.X;
            Board.Point fieldToPutSign = new Board.Point(0, 0);

            SignPuttingResult puttingResult = GetPuttingService().TryPutSign(board, signToPut, fieldToPutSign);

            Assert.IsTrue(puttingResult.IsPut);
        }

        private SignPuttingService GetPuttingService() => new SignPuttingService();
    }
}
