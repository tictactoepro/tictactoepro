﻿using NUnit.Framework;
using TicTacToeCore.Enums;
using TicTacToeCore.Models;

namespace TicTacToeCoreTests.Models
{
    [TestFixture]
    public class GameResultTests
    {
        [Test]
        public void Equals_GameResultsEmptyCtor_True()
        {
            GameResult gameResult1 = new GameResult();
            GameResult gameResult2 = new GameResult();

            Assert.AreEqual(gameResult1, gameResult2);
        }

        [Test]
        public void Equals_GameResultsWithPlayerSignValue_True()
        {
            GameResult gameResult1 = new GameResult(PlayerSign.X);
            GameResult gameResult2 = new GameResult(PlayerSign.X);

            Assert.AreEqual(gameResult1, gameResult2);
        }

        [Test]
        public void Equals_GameResultWithEmptyCtorAndGameResultWithSetValue_False()
        {
            GameResult gameResult1 = new GameResult();
            GameResult gameResult2 = new GameResult(PlayerSign.X);

            Assert.AreNotEqual(gameResult1, gameResult2);
        }

        [Test]
        public void Equals_GameResultWithSetValueAndGameResultWithSetDifferentValue_False()
        {
            GameResult gameResult1 = new GameResult(PlayerSign.O);
            GameResult gameResult2 = new GameResult(PlayerSign.X);

            Assert.AreNotEqual(gameResult1, gameResult2);
        }

        [Test]
        public void Equals_GameResultWithSetValueAndTableOfDifferentObject_False()
        {
            GameResult gameResult1 = new GameResult(PlayerSign.O);
            PlayerSign[] differentObject = new []{PlayerSign.O};

            Assert.AreNotEqual(gameResult1, differentObject);
        }
    }
}
