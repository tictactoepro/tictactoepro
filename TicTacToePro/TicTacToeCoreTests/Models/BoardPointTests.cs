﻿using System;
using NUnit.Framework;
using TicTacToeCore.Models;

namespace TicTacToeCoreTests.Models
{
    [TestFixture]
    public class BoardPointTests
    {
        [Test]
        public void SetX_InvalidX_ArgumentException()
        {
            Board.Point boardPoint = new Board.Point();
            Assert.Catch<ArgumentException>(() => { boardPoint.X = 5; });
        }

        [Test]
        public void SetY_InvalidY_ArgumentException()
        {
            Board.Point boardPoint = new Board.Point();
            Assert.Catch<ArgumentException>(() => { boardPoint.Y = 5; });
        }

        [Test]
        public void CtorWithParameters_InvalidXAndY_ArgumentException()
        {
            Board.Point boardPoint = new Board.Point();
            Assert.Catch<ArgumentException>(() => new Board.Point(5, 5));
        }

        [Test]
        public void SetX_ValidX_XValueIsSet()
        {
            Board.Point boardPoint = new Board.Point();
            int valueToSet = 2;
            boardPoint.X = valueToSet;
            Assert.AreEqual(valueToSet, boardPoint.X);
        }

        [Test]
        public void SetY_ValidY_XValueIsSet()
        {
            Board.Point boardPoint = new Board.Point();
            int valueToSet = 1;
            boardPoint.Y = valueToSet;
            Assert.AreEqual(valueToSet, boardPoint.Y);
        }

        [Test]
        public void CtorWithParameters_ValidInput_ValidBoardPoint()
        {
            int x = 1;
            int y = 2;
            Board.Point boardPoint = new Board.Point(x, y);
            Assert.IsTrue(boardPoint.X == x && boardPoint.Y == y);
        }
    }
}
